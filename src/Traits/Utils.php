<?php

namespace Techendeavors\EmailAutoDiscover\Traits;

trait Utils
{
    private static function debugOutput($status, $notice)
    {
        if (config('emailautodiscover')['debug']) {
            echo sprintf("[ %-5s ] %s", $status, $notice).PHP_EOL;
        }
    }


    private static function getUrl($url)
    {
        if (class_exists("\GuzzleHttp\Client")) {
            $clientConfig = [
                'allow_redirects'   => true,
                'connect_timeout'   => 10,
                'force_ip_resolve'  => 'v4',
                'headers'           => [
                    'Accept'        => 'text/xml',
                    'User-Agent'    => 'Techendeavors EmailAutoDiscover Composer Package',
                ],
                'http_errors'       => false,
                'timeout'           => 10,
                'verify'            => true,
            ];

            self::debugOutput("true", "Guzzle Client Found");
            try {
                $client = new \GuzzleHttp\Client($clientConfig);

                self::debugOutput(".....", "Requesting $url");
                $request = $client->get($url);
            } catch (Exception $e) {
                self::debugOutput("false", "Guzzle Error ".$e->getMessage());
                return (bool) false;
            }

            if ($request->getStatusCode() != "200") {
                self::debugOutput("false", "Requested URL returned ".$request->getStatusCode());
                return (bool) false;
            }

            $output = $request->getBody()->getContents();

            if (! self::isValidXml($output)) {
                return (bool) false;
            }
        }

        if (! class_exists("\GuzzleHttp\Client")) {
            self::debugOutput("false", "Guzzle Client Found");
            self::debugOutput("true", "Curl Client Found");
            self::debugOutput(".....", "Requesting $url");
            $curlHandle = curl_init($url);
            curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_TIMEOUT, 4);
            curl_setopt($curlHandle, CURLOPT_URL, $url);

            if (! $output = curl_exec($curlHandle)) {
                self::debugOutput("false", "Curl Error Occurred");
                curl_close($curlHandle);
                return (bool) false;
            };

            if (! self::isValidXml($output)) {
                return (bool) false;
            }
        }
        
        return $output;
    }

    private static function isValidXml($content)
    {
        $content = trim($content);
        if (empty($content)) {
            self::debugOutput("false", "isValidXML - No Content");
            return false;
        }
        //html go to hell!
        if (stripos($content, '<!DOCTYPE html>') !== false) {
            self::debugOutput("false", "isValidXML - HTML Content Detected");
            return false;
        }

        libxml_use_internal_errors(true);
        simplexml_load_string($content);
        $errors = libxml_get_errors();
        libxml_clear_errors();

        if ($errors) {
            self::debugOutput("false", "isValidXML - XML Errors Found");
        } else {
            self::debugOutput("true", "isValidXML - XML looks OK");
        }

        return empty($errors);
    }
    
}
