<?php

namespace Techendeavors\EmailAutoDiscover\Traits;

trait Checks
{
    public static function checkAutoConfigFile($domain, $email = null)
    {
        $url = 'https://autoconfig.'.$domain.self::AUTOCONFIG_PLAIN_PATH;

        if ($email) {
            $url = 'https://autoconfig.'.$domain.self::AUTOCONFIG_EMAIL_PATH.$email;
        }

        $output = json_encode(simplexml_load_string(self::getUrl($url)));

        if (empty($output)) {
            $url = 'http://autoconfig.'.$domain.self::AUTOCONFIG_PLAIN_PATH;

            if ($email) {
                $url = 'http://autoconfig.'.$domain.self::AUTOCONFIG_EMAIL_PATH.$email;
            }

            $output = json_encode(simplexml_load_string(self::getUrl($url)));
        }

        if (! empty($output)) {
            self::debugOutput("true", "AutoConfig file found at $url");
            return self::parseAutoConfig(json_decode($output, true), $url);
        }

        self::debugOutput("false", "AutoConfig file found at $url");
        return (bool) false;
    }

    public static function checkAutoDiscoverDomainFile($domain)
    {
        $url = 'https://autodiscover.'.$domain.self::AUTODISCOVER_PATH;

        $output = json_encode(simplexml_load_string(self::getUrl($url)));

        if (empty($output)) {
            $url = 'http://autodiscover.'.$domain.self::AUTODISCOVER_PATH;

            $output = json_encode(simplexml_load_string(self::getUrl($url)));
        }

        if (! empty($output)) {
            self::debugOutput("true", "AutoDiscover file found at $url");
            return json_decode($output, true);
        }
        
        self::debugOutput("false", "AutoDiscover file found at $url");
        return (bool) false;
    }

    public static function checkAutoDiscoverFile($domain)
    {
        $url = 'https://'.$domain.self::AUTODISCOVER_PATH;

        $output = json_encode(simplexml_load_string(self::getUrl($url)));

        if (empty($output)) {
            $url = 'http://'.$domain.self::AUTODISCOVER_PATH;

            $output = json_encode(simplexml_load_string(self::getUrl($url)));
        }

        if (! empty($output)) {
            self::debugOutput("true", "AutoDiscover file found at $url");
            return json_decode($output, true);
        }

        self::debugOutput("false", "AutoDiscover file found at $url");
        return (bool) false;
    }

    public static function checkMozilla($domain)
    {
        $url = self::MOZILLA_DB_URL.$domain;

        $results = self::getUrl($url);

        if (! empty($results)) {
            $output = json_decode(json_encode(simplexml_load_string($results)), true);

            self::debugOutput("true", 'Mozilla DB Results');
            return self::parseAutoConfig($output, $url);
        }

        self::debugOutput("false", 'Mozilla DB Results');
        return (bool) false;
    }

    public static function checkWellKnownFile($domain)
    {
        $url = 'https://'.$domain.self::WELL_KNOWN_PATH;
        $output = self::getUrl($url);

        if (empty($output)) {
            $url = 'http://'.$domain.self::WELL_KNOWN_PATH;
            $output = self::getUrl($url);
        }

        if (! empty($output)) {
            self::debugOutput("true", "Well-Known file found at $url");
            return self::parseAutoConfig(json_decode(json_encode(simplexml_load_string($output)), true), $url);
        }

        self::debugOutput("false", "Well-Known file found at $url");
        return (bool) false;
    }
}
