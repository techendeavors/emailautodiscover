<?php

namespace Techendeavors\EmailAutoDiscover\Traits;

trait DNS
{
    public function hasAutoConfigDomain($domain)
    {
        $dns = new \Techendeavors\DNSOverHttps\DNSOverHttps;
        $dns->domain('autoconfig.'.$domain);
        $dns->records(['A', 'AAAA', 'CNAME']);
        $results = $dns->check();

        if (empty($results)) {
            self::debugOutput("false", "autoconfig.$domain found");
        } else {
            self::debugOutput("true", "autoconfig.$domain found");
        }

        return (! empty($results));
    }

    public function hasAutoDiscoverDomain($domain)
    {
        $dns = new \Techendeavors\DNSOverHttps\DNSOverHttps;
        $dns->domain('autodiscover.'.$domain);
        $dns->records(['A', 'AAAA', 'CNAME']);
        $results = $dns->check();

        if (empty($results)) {
            self::debugOutput("false", "autodiscover.$domain found");
        } else {
            self::debugOutput("true", "autodiscover.$domain found");
        }

        return (! empty($results));
    }
}
