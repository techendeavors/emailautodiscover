<?php

namespace Techendeavors\EmailAutoDiscover\Traits;

trait Parse
{
    private static function parseAutoConfig($object, $url = null)
    {
        if (! empty($object)) {
            $obj                        = [];
            $obj['displayName']         = array_has($object, 'emailProvider.displayName')
                                        ? $object['emailProvider']['displayName']
                                        : null;
            $obj['displayShortName']    = array_has($object, 'emailProvider.displayShortName')
                                        ? $object['emailProvider']['displayShortName']
                                        : null;

            if (array_has($object, 'emailProvider.incomingServer')) {
                if (array_has($object, 'emailProvider.incomingServer.hostname')) {
                    $obj['incoming'][] = [
                        'type'              => array_has($object, 'emailProvider.incomingServer.@attributes.type')
                                            ? $object['emailProvider']['incomingServer']['@attributes']['type']
                                            : null,
                        'hostname'          => array_has($object, 'emailProvider.incomingServer.hostname')
                                            ? $object['emailProvider']['incomingServer']['hostname']
                                            : null,
                        'port'              => array_has($object, 'emailProvider.incomingServer.port')
                                            ? $object['emailProvider']['incomingServer']['port']
                                            : null,
                        'socketType'        => array_has($object, 'emailProvider.incomingServer.socketType')
                                            ? $object['emailProvider']['incomingServer']['socketType']
                                            : null,
                        'username'          => array_has($object, 'emailProvider.incomingServer.username')
                                            ? $object['emailProvider']['incomingServer']['username']
                                            : null,
                        'authentication'    => array_has($object, 'emailProvider.incomingServer.authentication')
                                            ? $object['emailProvider']['incomingServer']['authentication']
                                            : null
                    ];
                } else {
                    foreach ($object['emailProvider']['incomingServer'] as $incoming) {
                        $obj['incoming'][] = [
                            'type'              => array_has($incoming, '@attributes.type')
                                                ? $incoming['@attributes']['type']
                                                : null,
                            'hostname'          => array_has($incoming, 'hostname')
                                                ? $incoming['hostname']
                                                : null,
                            'port'              => array_has($incoming, 'port')
                                                ? $incoming['port']
                                                : null,
                            'socketType'        => array_has($incoming, 'socketType')
                                                ? $incoming['socketType']
                                                : null,
                            'username'          => array_has($incoming, 'username')
                                                ? $incoming['username']
                                                : null,
                            'authentication'    => array_has($incoming, 'authentication')
                                                ? $incoming['authentication']
                                                : null
                        ];
                    }
                }
            }
            if (array_has($object, 'emailProvider.outgoingServer')) {
                if (array_has($object, 'emailProvider.outgoingServer.hostname')) {
                    $obj['outgoing'][] = [
                        'type'              => array_has($object, 'emailProvider.outgoingServer.@attributes.type')
                                            ? $object['emailProvider']['outgoingServer']['@attributes']['type']
                                            : null,
                        'hostname'          => array_has($object, 'emailProvider.outgoingServer.hostname')
                                            ? $object['emailProvider']['outgoingServer']['hostname']
                                            : null,
                        'port'              => array_has($object, 'emailProvider.outgoingServer.port')
                                            ? $object['emailProvider']['outgoingServer']['port']
                                            : null,
                        'socketType'        => array_has($object, 'emailProvider.outgoingServer.socketType')
                                            ? $object['emailProvider']['outgoingServer']['socketType']
                                            : null,
                        'username'          => array_has($object, 'emailProvider.outgoingServer.username')
                                            ? $object['emailProvider']['outgoingServer']['username']
                                            : null,
                        'authentication'    => array_has($object, 'emailProvider.outgoingServer.authentication')
                                            ? $object['emailProvider']['outgoingServer']['authentication']
                                            : null
                    ];
                } else {
                    foreach ($object['emailProvider']['outgoingServer'] as $outgoing) {
                        $obj['outgoing'][] = [
                            'type'              => array_has($outgoing, '@attributes.type')
                                                ? $outgoing['@attributes']['type']
                                                : null,
                            'hostname'          => array_has($outgoing, 'hostname')
                                                ? $outgoing['hostname']
                                                : null,
                            'port'              => array_has($outgoing, 'port')
                                                ? $outgoing['port']
                                                : null,
                            'socketType'        => array_has($outgoing, 'socketType')
                                                ? $outgoing['socketType']
                                                : null,
                            'username'          => array_has($outgoing, 'username')
                                                ? $outgoing['username']
                                                : null,
                            'authentication'    => array_has($outgoing, 'authentication')
                                                ? $outgoing['authentication']
                                                : null
                        ];
                    }
                }
            }
            if (array_has($object, 'emailProvider.documentation')) {
                if (array_has($object, 'emailProvider.documentation.@attributes')) {
                    $obj['documenation'][] = [
                        'url'           => array_has($object, 'emailProvider.documentation.@attributes.url')
                                        ? $object['emailProvider']['documentation']['@attributes']['url']
                                        : null,
                        'description'   => array_has($object, 'emailProvider.documentation.descr')
                                        ? $object['emailProvider']['documentation']['descr']
                                        : null
                    ];
                } else {
                    foreach ($object['emailProvider']['documentation'] as $docs) {
                        $obj['documenation'][] = [
                            'url'           => array_has($docs, '@attributes.url')
                                            ? $docs['@attributes']['url']
                                            : null,
                            'description'   => array_has($docs, 'descr')
                                            ? $docs['descr']
                                            : null
                        ];
                    }
                }
            }

            $obj['source'] = $url;

            return $obj;
        }

        return (bool) false;
    }
}
