<?php

namespace Techendeavors\EmailAutoDiscover;

use Illuminate\Support\Collection;
use Techendeavors\EmailAutoDiscover\Traits\Checks;
use Techendeavors\EmailAutoDiscover\Traits\DNS;
use Techendeavors\EmailAutoDiscover\Traits\Parse;
use Techendeavors\EmailAutoDiscover\Traits\Utils;

class EmailAutoDiscover
{
    use Checks, DNS, Parse, Utils;

    private const MOZILLA_CHECK         = true;
    private const DNS_CHECK             = true;
    private const SERVER_CHECK          = true;
    private const GUESS_CHECK           = true;
    private const MOZILLA_DB_URL        = "https://autoconfig.thunderbird.net/v1.1/";
    private const WELL_KNOWN_PATH       = "/.well-known/autoconfig/mail/config-v1.1.xml";
    private const AUTOCONFIG_EMAIL_PATH = "/mail/config-v1.1.xml?emailaddress=";
    private const AUTOCONFIG_PLAIN_PATH = "/mail/config-v1.1.xml";
    private const AUTODISCOVER_PATH     = "/autodiscover/autodiscover.xml";

    protected $config;
    protected $enabled;
    protected $key;

    /**
     * Classes which have a constructor method call this method on each newly-created object, 
     * so it is suitable for any initialization that the object may need before it is used.
     */
    public function __construct()
    {
        $this->config   = config('emailautodiscover');
        $this->enabled  = $this->config['enabled']         ?: self::ENABLED;
    }

    public function check($domain, $email = null)
    {
        if (config('emailautodiscover')['mozilla']) {
            self::debugOutput('.....', "Checking Mozilla DB");
            if ($return = self::checkMozilla($domain)) {
                return $return;
            }
        }

        if (config('emailautodiscover')['autoconfig']) {
            self::debugOutput('.....', "Checking AutoConfig Files on their own domain");
            if (self::hasAutoConfigDomain($domain) && ($return = self::checkAutoConfigFile($domain, $email))) {
                return $return;
            }
        }

        if (config('emailautodiscover')['exchange']) {
            self::debugOutput('.....', "Checking AutoDiscover Files on their own domain");
            if (self::hasAutoDiscoverDomain($domain) && ($return = self::checkAutoDiscoverDomainFile($domain))) {
                return $return;
            }
        }

        if (config('emailautodiscover')['autoconfig']) {
            self::debugOutput('.....', "Checking AutoConfig file on base domain");
            if ($return = self::checkWellKnownFile($domain)) {
                return $return;
            }
        }

        if (config('emailautodiscover')['exchange']) {
            self::debugOutput('.....', "Checking AutoDiscover file on base domain");
            if ($return = self::checkAutoDiscoverFile($domain)) {
                return $return;
            }
        }

        if (config('emailautodiscover')['guess']) {
            self::debugOutput('.....', "Guessing configuration");
            if ($return = self::guessConfig($domain)) {
                return false;
            }
        }

        return (bool) false;
    }

    public function guessConfig($domain)
    {
        // TODO: Try to guess the configuration, by trying common server names like imap.<domain>, smtp.<domain>, 
        // mail.<domain> etc., and, when a mail server answers, checking whether it supports SSL, STARTTLS and 
        // encrypted passwords (CRAM-MD5)
    }
}
