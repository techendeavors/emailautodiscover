<?php

namespace Techendeavors\EmailAutoDiscover;

use Illuminate\Support\Facades\Facade;

class EmailAutoDiscoverFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'dnsoverhttps';
    }
}
