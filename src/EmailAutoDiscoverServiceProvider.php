<?php

namespace Techendeavors\EmailAutoDiscover;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Container\Container;

class EmailAutoDiscoverServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $source = realpath($raw = __DIR__.'/../config/emailautodiscover.php') ?: $raw;

        $this->publishes([$source => config_path('emailautodiscover.php')], 'config');
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $source = realpath($raw = __DIR__.'/../config/emailautodiscover.php') ?: $raw;

        $this->app->alias('emailautodiscover', EmailAutoDiscover::class);

        $this->mergeConfigFrom($source, 'emailautodiscover');

        $this->app->singleton('emailautodiscover', function (Container $app) {
            return new EmailAutoDiscover();
        });        
    }
}
