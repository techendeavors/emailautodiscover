<?php

namespace Techendeavors\EmailAutoDiscover\Tests;

use Techendeavors\EmailAutoDiscover\EmailAutoDiscoverFacade;
use Techendeavors\EmailAutoDiscover\EmailAutoDiscoverServiceProvider;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

class TestCase extends OrchestraTestCase
{
    /**
     * Load package service provider
     * @param  \Illuminate\Foundation\Application $app
     * @return Techendeavors\EmailAutoDiscover\EmailAutoDiscoverServiceProvider
     */
    protected function getPackageProviders($app)
    {
        return [EmailAutoDiscoverServiceProvider::class];
    }
    /**
     * Load package alias
     * @param  \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageAliases($app)
    {
        return [
            'EmailAutoDiscover' => EmailAutoDiscoverFacade::class,
        ];
    }
}
