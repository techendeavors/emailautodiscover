<?php

return [

/**
 * EmailAutoDiscover Settings
 */

    'enabled' => env(strtoupper('emailautodiscover') . '_ENABLED', true),
    'debug' => true,
    
    'autoconfig' => true,
    'exchange' => false,
    'guess' => false,
    'mozilla' => true,
        
];
