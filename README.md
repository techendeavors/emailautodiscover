# EmailAutoDiscover

## Takes an email address or domain and attempts to get the autoconfiguration settings necessary to configure an email client. 

[![Latest Version on Packagist](https://img.shields.io/packagist/v/techendeavors/emailautodiscover.svg?style=flat-square)](https://packagist.org/packages/techendeavors/emailautodiscover)
[![Total Downloads](https://img.shields.io/packagist/dt/techendeavors/emailautodiscover.svg?style=flat-square)](https://packagist.org/packages/techendeavors/emailautodiscover)

### To Do

- [ ] Check for MX records first
- [ ] Make sure all variables are set by constants, but can be overridden with config files
- [ ] Check for loading of DNSOverHttps before using it
- [ ] Handle various Guzzle/Curl errors more cleanly by catching exceptions
- [ ] Fix Exchange autoconfigure detection
- [ ] Create a configuration guesswork system
  - [ ] Check MX records
  - [ ] Check SPF records
  - [ ] Check for common mail server 3rd level domains
  - [ ] Connect to possible 3rd level domains at common ports
  - [ ] Look for common webmail 3rd level domains and try to determine webmail platform

### Installation

You can install the package via composer:

```bash
composer require techendeavors/emailautodiscover
```

### Usage

``` 
>>> $test = new EmailAutoDiscover;
=> Techendeavors\EmailAutoDiscover\EmailAutoDiscover {#2863}
>>> $test->check('gmail.com')
=> [
     "displayName" => "Google Mail",
     "displayShortName" => "GMail",
     "incoming" => [
       [
         "type" => "imap",
         "hostname" => "imap.gmail.com",
         "port" => "993",
         "socketType" => "SSL",
         "username" => "%EMAILADDRESS%",
         "authentication" => [
           "OAuth2",
           "password-cleartext",
         ],
       ],
       [
         "type" => "pop3",
         "hostname" => "pop.gmail.com",
         "port" => "995",
         "socketType" => "SSL",
         "username" => "%EMAILADDRESS%",
         "authentication" => "password-cleartext",
       ],
     ],
     "outgoing" => [
       [
         "type" => "smtp",
         "hostname" => "smtp.gmail.com",
         "port" => "465",
         "socketType" => "SSL",
         "username" => "%EMAILADDRESS%",
         "authentication" => [
           "OAuth2",
           "password-cleartext",
         ],
       ],
     ],
     "documenation" => [
       [
         "url" => "http://mail.google.com/support/bin/answer.py?answer=13273",
         "description" => "How to enable IMAP/POP3 in GMail",
       ],
       [
         "url" => "http://mail.google.com/support/bin/topic.py?topic=12806",
         "description" => "How to configure email clients for IMAP",
       ],
       [
         "url" => "http://mail.google.com/support/bin/topic.py?topic=12805",
         "description" => "How to configure email clients for POP3",
       ],
       [
         "url" => "http://mail.google.com/support/bin/answer.py?answer=86399",
         "description" => "How to configure TB 2.0 for POP3",
       ],
     ],
     "source" => "https://autoconfig.thunderbird.net/v1.1/gmail.com",
   ]
>>> 
>>> $test->check('techendeavors.com')
=> [
     "displayName" => "Techendeavors Mail",
     "displayShortName" => "Techendeavors",
     "incoming" => [
       [
         "type" => "imap",
         "hostname" => "mail.techendeavors.com",
         "port" => "143",
         "socketType" => "STARTTLS",
         "username" => "%EMAILADDRESS%",
         "authentication" => "password-cleartext",
       ],
     ],
     "outgoing" => [
       [
         "type" => "smtp",
         "hostname" => "mail.techendeavors.com",
         "port" => "587",
         "socketType" => "STARTTLS",
         "username" => "%EMAILADDRESS%",
         "authentication" => "password-cleartext",
       ],
     ],
     "source" => "https://autoconfig.techendeavors.com/mail/config-v1.1.xml",
   ]
>>> 
>>> $test->check('example.com')
=> false
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

### Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please use the [issue tracker](https://gitlab.com/techendeavors/emailautodiscover)

### Credits

- [Techendeavors](https://gitlab.com/techendeavors)
- [Contributors and Credits](CONTRIBUTORS.md)



### License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
